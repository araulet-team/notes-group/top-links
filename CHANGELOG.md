# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

<!--- next entry here -->

## 0.1.1
2019-10-06

### Fixes

- **git:** typo in images path (eab21fb203050ac3a0e89a905c31c8f5db1dd3cf)

## 0.1.0
2019-10-06

### Features

- add containers links (aaa1ff3477e3613d24818aed042d78b0a9938293)
- **git:** add notes for git workflow + pipelines (3ee425389ba2231a7c4af3a7dfa5281cabe948ed)

### Fixes

- **ci:** release notes (7e7088e5254bc57ffe4ee29b39befa7717249425)

